import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class ApiService {
  public url:string='https://jsonplaceholder.typicode.com/users';
  public userDetail=new BehaviorSubject([]);
  constructor(private http:HttpClient) { }

  getData():Observable<any> {
    return this.http.get(this.url);
  }

}

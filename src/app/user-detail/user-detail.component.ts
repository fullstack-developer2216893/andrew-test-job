import { Component, OnInit} from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  singleUserInfo=[];
  constructor(private apiservice:ApiService,private activeRoute:ActivatedRoute,private spinner:NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    const id:any=this.activeRoute.snapshot.paramMap.get('id');
    this.apiservice.getData().subscribe(res=>{
      this.singleUserInfo=res[id-1];
      this.spinner.hide();
      
    });
    
   
  }

}

import { Component, OnInit} from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  private userData:[]=[];
  constructor(private apiService:ApiService,private router:Router,private spinner:NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.apiService.getData().subscribe(res=>{
      this.userData=res;
      this.spinner.hide();
    });
    
  }

  openDetailPage(id:string){
    this.router.navigate(['single-user',id]);
  }

}
